<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
     <!-- Fonts -->
     <link rel="dns-prefetch" href="//fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

     <!-- Styles -->
     <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="css/style.css" rel="stylesheet">
    {{-- Bootstrap--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

   
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"  ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script>
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.map"></script> --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Imobiliária J</title>
</head>
<body >
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="col-sm-2">
            <a href="{{ route('listPropertie') }}" >
                <img src="{{ asset('photos/house.png') }}"  alt="logo" class="d-inline-block rounded float-left " width="140" height="110" >
            </a>
        </div>
        <div class="col-sm-2">
            {{-- <h1 class="justify-content-end">Ville Imob</h1> --}}
            <img src="{{asset('photos/Ville Imob.png')}}" alt="letras" >
        </div>
        {{-- botao  para navegação em mobile--}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
        </button>
        
    </nav>
    <div class="p-3  bg-warning bg-gradient text-dark">
        @yield('base')
    </div>
    <div class="p-3 mb-2 text-dark">
        <div class="container">
            <h5>Informações da Empresa</h5>
            <button type="button" class="btn btn-info">Fale Conosco</button>
        </div>
    </div>
</body>
</html>