@extends('templates/baseAplication')


@section('base')
    <div class="container">
    <h3 class="card-title titulo">REGISTRAR IMÓVEL</h3>
    <hr style="background-color: #6937FF">
        <div class="row">
                <div class="card col-md-12 contorno-btn" >
                    <div class="card-body">
                        <br><br>
                        @if (session('errorImage'))
                            <div class="alert alert-danger col-md-12" style="width:100%; height:50px;">
                                <span>{{session('errorImage')}}</span>
                            </div>
                        @endif

                        @if (session('erroCode'))
                            <div class="alert alert-danger col-md-12" style="width:100%; height:50px;">
                                <span>{{session('erroCode')}}</span>
                            </div>
                        @endif
                        <p></p>
                            <form class="form-horizontal" method="post" action="{{ route('savePropertie') }}" enctype="multipart/form-data">
                                @csrf

                                {{-- Campo: Titulo  --}}
                                <div class="form-group row">
                                    <label class="control-label col-sm-4 text-sm-right" for="title">Título:</label>
                                    <div class="col-sm-6">
                                        <input required type="text" class="form-control" name="title" id="title" placeholder="Coloque um Título">
                                    </div>
                                </div>
                                <p></p>

                                {{-- Campo: Código  --}}
                                <div class="form-group row">
                                    <label class="control-label col-sm-4 text-sm-right" for="code" aria-required="true">Cógigo:</label>
                                    <div class="col-sm-6">
                                        <input required type="text" class="form-control" name="code" id="code" placeholder="Coloque um Código de até 10 Caractares">
                                    </div>
                                </div>

                                <p></p>
                                {{-- Campo: Upload Imagem --}}
                                <div class="input-group input-group-sm mb-3 row">
                                    <label class="control-label col-sm-5 text-sm-right" for="photo" >Foto:</label>
                                    <div class="custom-file col-sm-3">
                                        <input type="file" class="custom-file-input" id="customFile" name="photo" id="image">
                                    </div>
                                </div>


                                {{-- Campo: Categoria --}}
                                <div class="form-group row">
                                    <label class="control-label col-sm-4 text-sm-right" for="course">Categoria:</label>
                                    <div class="col-sm-6">
                                        <select name="categoria" class="form-control" required>
                                            <option selected value="" class="text-default">Selecione a categoria</option>
                                            <option value="Apartamento">Apartamento</option>
                                            <option value="Casa">Casa</option>
                                            <option value="Terreno">Terreno</option>
                                        
                                        </select>
                                    </div>
                                </div>
                                <p></p>
                                {{-- Campo: Valor --}}
                                <div class="form-group row">
                                    <label class="control-label col-sm-4 text-sm-right" for="value">Valor(Opcional):</label>
                                    <div class="col-sm-6">                  
                                        <input type="number"  name="value" min="0" value="0" step=".01" placeholder="Digite um valor( Opcional) ">
                                    </div>
                                </div>
                                
                                <p></p>


                                {{-- Campo: Descricao --}}
                                <div class="form-group row">
                                    <label for="description" class="control-label col-sm-4 text-sm-right">Descrição:</label>
                                    <div class="col-sm-6">
                                        <textarea id="summernote" class="form-control col-sm-10" id="note" name="note" rows="3"></textarea>
                                    </div>
                                </div>


                                <p></p>    


                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <a href="{{ route('listPropertie') }}" name="cancelar" class="btn btn-danger">Cancelar</a>
                                        <button type="submit" name="registrar" class="btn btn-success">Registrar</button>
                                    </div>
                                </div>

                            </form>

                    </div>
                </div>
        </div>
    </div>
@endsection