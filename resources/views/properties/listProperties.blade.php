@extends('templates/baseAplication')


@section('base')

        <div class="container">
            <div class="card-title titulo"><h2>LISTA DE IMÓVEIS</h2></div>
            <hr style="background-color: #640fc4">
            <div class="panel panel-default">
                <a type="button" href="{{ route('registerPropertie') }}"
                class="btn btn-success btn-size" style="btn-display:block;  ">Adicionar um Imóvel</a>
                <div class="row container">
                    <p></p>
                    {{-- Mensagens --}}
                    @if (session('success'))
                    <div class="alert alert-success col-md-12" style="width:100%; height:50px;">
                        <span>{{session('success')}}</span>
                    </div>
                    @endif
                    @if (session('deleted'))
                    <div class="alert alert-dark col-md-12" style="width:100%; height:50px;">
                        <span>{{session('deleted')}}</span>
                    </div>
                    @endif
                    @if (session('edited'))
                    <div class="alert alert-success col-md-12" style="width:100%; height:50px;">
                        <span>{{session('edited')}}</span>
                    </div>
                    @endif
                    
                    <p></p>
                    @foreach ($properties as $item)
                    <div class="col-lg-3">
                        <div class="card style="width: 24rem;"">
                            <img src="image/photos/{{ $item->photo}}" alt="{{ $item->photo }}" class="card-img-top card-img" />
                            <div class="card-body">
                                <p class="card-text">
                                    {{ $item->title}} | {{$item->code}}
                                </p>
                                <p>
                                    <b>Value: </b> 
                                    @php
                                        if($item->value == 0){
                                            echo "Não informado";
                                        }
                                        else echo "R$".$item->value;
                                        @endphp
                                </p>
                                <h5 class="card-title">
                                    {{ $item->note }}
                                </h5>
                                <a  href="{{ route('editPropertie', $item->id) }}" > Editar</a>
                            </div>
                        </div>
                    </div>
                    
                    @endforeach
                    
                    
                    
                    
                    
                </div>
                
            </div>
        </div>
       
    @endsection