<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PropertieController;


/**
 * 
 * Rotas para os imóveis
 */
Route::get('/', [PropertieController::class, 'index'])->name('listPropertie');
Route::get('/properties/registerPropertie', [PropertieController::class, 'create'])->name('registerPropertie');
Route::post('/properties/registerPropertie', [PropertieController::class, 'store'])->name('savePropertie');
Route::get('properties/{id}', [PropertieController::class, 'edit'])->name('editPropertie');
Route::put('properties/{id}', [PropertieController::class, 'update'])->name('updatedPropertie');
