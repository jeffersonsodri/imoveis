<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Propertie;
use Illuminate\Support\Facades\DB;



class PropertieController extends Controller
{
    /**
     *
     */
    public function index()
    {
        $properties = DB::table('properties')->get();
    
        return view('properties.listProperties', compact('properties'));

    }

    /**
     *
     */
    public function create()
    {

        return view('properties.registerProperties');
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        
        $code = $dados['code'];
        $verifCode = DB::table('properties')->where("code", $code)->first();
        
        if($verifCode != null ){
            return redirect()->back()->with('erroCode', 'Digite Outro Código!');
        }
        else{

        
            /**
             * Verifica se a imagem é válida
             */
            if($request->hasfile('photo') && $request->file('photo')->isValid() ){
                $file = $request->file('photo');
                $extantion = $file->getClientOriginalExtension(); //Extenção da imagem
                $filename = time() . '.' . $extantion;
                $file->move('image/photos', $filename);
                $dados['photo'] = $filename;

                if(!$filename){
                    return redirect()->back()->with('errorImage', 'Falha ao fazer o upload da Imagem');
                }
            }
            else {
                return redirect()->back()->with('errorImage', 'Falha ao fazer o upload da Imagem');
            }



            Propertie::create($dados);
            

            return redirect()->route('listPropertie')->with('success', 'Imóvel cadastrado com sucesso!');

        }
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $propertie = DB::table('properties')->where('id', $id)->first();
        
        return view('properties.editProperties', compact('propertie'));

    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dados = $request->all();
        
        $code = $dados['code'];
        $verifCode = DB::table('properties')->where("code", $code)->first();
      
        if($verifCode != null && $verifCode->code != $code){
            return redirect()->back()->with('erroCode', 'Digite Outro Código!');
        }
        else{

        
            /**
             * Verifica se a imagem é válida
             */
            if($request->hasfile('photo') && $request->file('photo')->isValid() ){
                $file = $request->file('photo');
                $extantion = $file->getClientOriginalExtension(); //Extenção da imagem
                $filename = time() . '.' . $extantion;
                $file->move('image/photos', $filename);
                $dados['photo'] = $filename;

                if(!$filename){
                    return redirect()->back()->with('errorImage', 'Falha ao fazer o upload da Imagem');
                }
            }
           
            $p = Propertie::find($id);
            
            $p->update($dados);
            return redirect()->route('listPropertie')->with('success', 'Imóvel editado com sucesso!');
            
        }
    }
    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
